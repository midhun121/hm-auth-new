var Boom         = require('boom');
var Hoek         = require('hoek');
exports.register = function (plugin, options, next) {
    plugin.auth.scheme('hm-auth', function (server, options) {

        Hoek.assert(options, 'Missing bearer auth strategy options');
        //Hoek.assert(options.dbConfig, 'Missing Database Configurations');
        //Hoek.assert(options.dbConfig.instance, 'Missing Database instance');
        Hoek.assert(typeof options.validateFunc === 'function', 'options.validateFunc must be a valid function in bearer scheme');


        options.accessTokenName = options.accessTokenName || "authToken";
        var settings            = Hoek.clone(options);

        var scheme = {
            authenticate: function (request, reply) {

                var req                = request.raw.req;
                var authorizationToken = req.headers.authorization;

                if (!authorizationToken) {
                    authorizationToken = request.query[settings.accessTokenName]
                }
                if (!authorizationToken) {
                    return reply(Boom.unauthorized(null, 'Token Invalid'));
                }
                /*
                 validateFunction(authorizationToken, options, function (err, result) {
                 if (err) {

                 return reply(Boom.unauthorized('Bad token', 'Bearer'), {credentials: credentials});

                 } else {
                 return reply(null, result);
                 }
                 });
                 */
                settings.validateFunc.call(request, authorizationToken, function (err, result) {
                    if (err) {
                        if (typeof err !== 'object') {
                            err = Boom.unauthorized('Bad token', 'Bearer')
                        }
                        return reply(err, {credentials: result});
                        // return reply(err, { credentials: credentials, log: { tags: ['auth', 'hmauth'], data: err } });
                    }

                    if (!result
                        || typeof result !== 'object') {
                        return reply(Boom.badImplementation('Bad token string received for hmauth auth validation'), {log: {tags: 'token'}});
                    }

                    return reply.continue({credentials: result});
                });
            }
        };

        return scheme;
    });
    next();
};

exports.register.attributes = {
    pkg: require('../package.json')
};


var validateFunction = function (token, options, callback) {
    if (options.dbConfig.type == "ES") {
        return callback("Do Be Implemented ES");
    } else {
        options.dbConfig.instance.get('SSID:' + token, function (err, result) {
            if (err)
                return callback(err);
            else
                return callback(null, result);
        });
    }

};